This lib modification was used to provide a solution to use Shadow with an NVIDIA GPU on linux. Now there's means to use Shadow with NVIDIA differently :

- Using [Shadow Beta Client](https://update.shadow.tech/launcher/preprod/linux/ubuntu_18.04/ShadowBeta.AppImage) or [Shadow Alpha Client](https://update.shadow.tech/launcher/testing/linux/ubuntu_18.04/ShadowAlpha.AppImage), official NVIDIA driver (>= 410.48) and in Launcher set **Decoder** to **FFmpeg**. 

# [DEPRECATED] libva-vdpau-driver - Shadow Edition

## Introduction

This patch was submited by [Arekinath](https://github.com/arekinath/libva-vdpau-driver) and makes possible NVIDIA serie **10** / **20** (& some older **GTX 560M**, **660**, **GT 750M**, **GTX 760**, **GTX 765M**, **780**, **820M**, **950M**, **960**, **970(M)**, **GT 1030**, **Quadro K1100M**, **Quadro K2100M**, **Quadro K2200**) with Shadow, you can find more details from our [community website](https://nicolasguilloux.github.io/blade-shadow-beta/)

## Requirements

 - Xorg
 - An NVIDIA serie **10**/**20** to **work** (**GT** / **GTX** / **RTX**).
 - Recent release of **official** NVIDIA driver
 - Disabled/Blacklisted opensource **nouveau** driver

## Known Issues

- Wayland **not** supported
- H265/HEVC **not** supported
- Doesn't work on **Nvidia 980M**
- Potential performance issues when using resolution **higher** than **1920x1080@60hz**
- [Black Screen] It can be rolled back to official (workaround: reinstall patched package and **sudo apt-mark hold vdpau-va-driver:amd64**)
- May take several "start" to work...
- If you get L:100 in Shadow, this hack may contains a race condition that prevent it to work if another app/game is using GPU (close all apps using GPU(internet browsers/discord/spotify...) 
- Enable "Low configuration" mode in Shadow Launcher (to disable CEF overlay (QuickMenu))
- Start appimage using `--no-sandbox`

## Download

### Ubuntu


~~wget https://gitlab.com/aar642/libva-vdpau-driver/-/jobs/205841211/artifacts/raw/vdpau-va-driver_0.7.4-6ubuntu1_amd64.deb~~
~~sudo dpkg -i vdpau-va-driver_0.7.4-6ubuntu1_amd64.deb~~
~~sudo apt-mark hold vdpau-va-driver:amd64~~


### Arch / Manjaro (Maintained by @Ludestru#3474, thanks to him!)

~~[AUR page](https://aur.archlinux.org/packages/libva-vdpau-driver-shadow-nvidia/)~~

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)

## Useful ressources :

- [NVidia VDPAU support](http://us.download.nvidia.com/XFree86/Linux-x86_64/430.40/README/vdpausupport.html)
- [VDPAU Documentation](https://vdpau.pages.freedesktop.org/libvdpau/)
- [vdpau-driver source](https://github.com/freedesktop/vdpau-driver)

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Shadow](https://shadow.tech).

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge
